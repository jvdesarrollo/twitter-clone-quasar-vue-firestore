import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";



export const firebaseConfig = {
  apiKey: "AIzaSyCp9G8hsuF7hrbEyvhOv_qtvs8tEnP7mrM",
  authDomain: "qwitter-d26ce.firebaseapp.com",
  projectId: "qwitter-d26ce",
  storageBucket: "qwitter-d26ce.appspot.com",
  messagingSenderId: "866520892385",
  appId: "1:866520892385:web:41a7b8939356a6601e08a0",
};

// init firebase app
const firebase = initializeApp(firebaseConfig);
export const dataBase = getFirestore(firebase);
